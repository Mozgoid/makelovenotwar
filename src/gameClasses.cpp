#include "cinder/app/AppNative.h"
#include "cinder/Rand.h"

#include "gameClasses.h"

#include <string>
#include <assert.h>

#include <amp.h>

using namespace ci;

extern std::vector<game::DrawJob> g_drawJobs;

namespace game 
{

AnimationInfo::AnimationInfo(const ci::XmlTree& xml)
{
    auto createVect = [](const std::string& str)
    {
        std::stringstream ss(str);
        float x; ss >> x;
        float y; ss >> y;
        return Vec2f(x, y);
    };

    frameCount = xml.getAttributeValue<unsigned>( "frameCount" );
    frameTime = 1.0f / xml.getAttributeValue<float>( "fps" );
    frameSize = createVect(xml.getAttributeValue<std::string>("frameSize"));
    textureOffset = createVect(xml.getAttributeValue<std::string>("offset"));
}

//==============================================================================
//                             Animation
//==============================================================================

Animation::Animation()
{

}


Animation::Animation(AnimationInfo* anim)
    : _anim(anim)
    , _frame(0)
    , _time(Rand::randFloat(1000.0f))
{
}


void Animation::update( float dt )
{
    _time += dt;
    auto timeFromFrameStart = _time - _frame * _anim->frameTime;
    if (timeFromFrameStart < _anim->frameTime)
    {
        return;
    }
    else
    {
        const auto pastFrames = (int)(timeFromFrameStart / _anim->frameTime);
        _frame = _frame + pastFrames;
        timeFromFrameStart = _time - _frame * _anim->frameTime;
        _frame %= _anim->frameCount;
        _time = _frame * _anim->frameTime + timeFromFrameStart;
    }
}


void Animation::createDrawTasks(ci::Vec2f pos) const
{
    auto topLeft = ci::Vec2f(_frame * _anim->frameSize.x, 0) 
        + _anim->textureOffset;

    const auto textureArea = ci::Area(topLeft, topLeft + _anim->frameSize);
    pos.y -= _anim->frameSize.y;
    const auto renderRect = ci::Rectf(pos, pos + _anim->frameSize);
    g_drawJobs.emplace_back(textureArea, renderRect);
}


//==============================================================================
//                                Game
//==============================================================================

static const float CLEAN_PERIOD = 10.0f;

Game::Game()
    : _hippiePoints(0)
    , _armyPoints(1000)
    , _pointsToWar(0)
    , _timeFromLastSoldiersAppear(0)
    , _timeToCleanup(CLEAN_PERIOD)
{
}


void Game::init()
{
    _defaultIndivids[ConflictSide_e::Hippie];
    _defaultIndivids[ConflictSide_e::Army];

    XmlTree doc( app::loadAsset( "units.xml" ) );
    auto& children = doc.getChild("units").getChildren();
    _animations.reserve(children.size());

    for(auto& c : children)
    {
        createDefaultIndivids(*c.get());
    }
}


void Game::createDefaultIndivids(const ci::XmlTree& xml)
{
    Individ i;
    i.position = Vec2f(-100.0f, 0.0f);
    i.animIndex = 0;

    auto sideStr = xml.getAttributeValue<std::string>( "side" );
    i.side = sideStr == "army" ? ConflictSide_e::Army : ConflictSide_e::Hippie;

    auto typeStr = xml.getAttributeValue<std::string>( "type" );
    if(typeStr == "vehicle")
    {
        i.type =  Individ::Type_e::Vehicle;
    }
    else if(typeStr == "hero")
    {
        i.type = Individ::Type_e::Hero;
    }
    else
    {
        i.type = Individ::Type_e::Human;
    }

    i.speed = xml.getAttributeValue<float>( "speed" );
    i.radius = xml.getAttributeValue<float>( "radius" );

    if(i.side == ConflictSide_e::Army)
    {
        i.speed *= -1.0f;
    }

    auto newAnimInfo = new AnimationInfo(xml.getChild("walk"));
    auto newAnimInfo2 = new AnimationInfo(xml.getChild("happy"));
    i.animation[0] = Animation(newAnimInfo);
    i.animation[1] = Animation(newAnimInfo2);

    _defaultIndivids[i.side][i.type].push_back(i);

    _animations.emplace_back(newAnimInfo);
    _animations.emplace_back(newAnimInfo2);
}


void Game::update( float dt )
{
//     for( unsigned i = 0, end = _individuals.size(); i < end; ++i)
//     {
//         auto& individ = _individuals[i];
//         individ.animation[individ.animIndex].createDrawTasks(individ.position);
//     }
//     return;

    static float ARMY_APPEAR_TIME = 0.3f;
    static float HIPPIE_POINT_SPEED = 100.0f;
    _hippiePoints += HIPPIE_POINT_SPEED * dt;

    _timeFromLastSoldiersAppear += dt;
    if(_armyPoints > 0.0f
        && Rand::randFloat(_timeFromLastSoldiersAppear) > ARMY_APPEAR_TIME)
    {
        _timeFromLastSoldiersAppear = 0.0f;
        addArmySquad();
    }

    std::lock_guard<std::mutex> lck(_gameMutex);

    for( unsigned i = 0, end = _individuals.size(); i < end; ++i)
    {
        auto& individ = _individuals[i];


        individ.animation[individ.animIndex].update(dt);
        individ.animation[individ.animIndex].createDrawTasks(individ.position);

        if(individ.side == ConflictSide_e::Neytral)
        {
            continue;
        }

        individ.position.x += individ.speed * dt;

        for( unsigned j = i; j < end; ++j)
        {
            auto& other = _individuals[j];

            const auto maxDiastance = (other.radius + individ.radius) * 0.5f;
            if((other.position.y - individ.position.y) > maxDiastance )
            {
                break;
            }
            else if(other.side != ConflictSide_e::Neytral
                    && other.side != individ.side
                    && abs(individ.position.x - other.position.x) <= maxDiastance)
            {
                individ.side = ConflictSide_e::Neytral;
                individ.animIndex = 1;
                other.side = ConflictSide_e::Neytral;
                other.animIndex = 1;
                break;
            }
        }
    }

    _timeToCleanup -= dt;

    if(_timeToCleanup < 0.0f)
    {
        _timeToCleanup = CLEAN_PERIOD;

        const auto border = 100;
        const auto leftBorder = -border;
        const auto rightBorder = app::getWindowWidth() + border;

        auto it = std::remove_if(_individuals.begin(), _individuals.end(), 
            [rightBorder, leftBorder]( const Individ& i)
        {
            return i.position.x < leftBorder || i.position.x > rightBorder;
        });

        _individuals.erase(it, _individuals.end());
    }

}


void Game::tryAddHippieSquad(ci::Vec2f position )
{
    if(position.x > app::getWindowWidth() / 2)
    {
        return;
    }

    int type = abs(Rand::randInt()) % (int)Individ::Type_e::TypeCount;
    //magic balance numbers
    auto normalHippie = 0;
    switch (type)
    {
    case game::Individ::Human:
        normalHippie = 7;
        break;
    case game::Individ::Vehicle:
        normalHippie = 3;
        break;
    case game::Individ::Hero:
        normalHippie = 5;
        break;
    default:
        break;
    };

    //for(int i = 0; i < 100; ++i) //just for fun
        add(ConflictSide_e::Hippie, normalHippie, (Individ::Type_e)type, position);
}


void Game::addArmySquad()
{
    auto width = (float)app::getWindowWidth();
    auto height = (float)app::getWindowHeight();
    auto squadCenter = Vec2f(
        width + 50/*Rand::randFloat( width/2.0f, width )*/,Rand::randFloat(height) + 30);

    auto type = Individ::Human;
    auto randNumber = Rand::randFloat(5.0f);
    if(randNumber > 3.5f)
    {
        type = Individ::Hero;
    }    
    else if(randNumber > 2.8f)
    {
        type = Individ::Vehicle;
    }

    auto normalSoldiers = 0;
    switch (type)
    {
    case game::Individ::Human:
        normalSoldiers = 10;
        break;
    case game::Individ::Vehicle:
        normalSoldiers = 2;
        break;
    case game::Individ::Hero:
        normalSoldiers = 6;
        break;
    default:
        break;
    };

    //for(int i = 0; i < 100; ++i) //just for fun
        add(ConflictSide_e::Army, normalSoldiers, type, squadCenter);
    //_armyPoints -= 100;
}



void Game::add(ConflictSide_e side, unsigned count
    , Individ::Type_e specialGuest, ci::Vec2f center)
{
    std::lock_guard<std::mutex> lck(_gameMutex);

    _individuals.reserve(_individuals.size() + count + 1);

    auto soldierLineOffset = _defaultIndivids[side][Individ::Human][0].radius;

    for(unsigned i = 0; i < count; ++i)
    {
        auto& humans = _defaultIndivids[side][Individ::Human];
        auto newIndivid = humans[Rand::randInt(0, humans.size())];

        auto  pos = (side == ConflictSide_e::Army)
            ? Vec2f(soldierLineOffset, soldierLineOffset * 0.3f) * i
            : Vec2f(
            Rand::randFloat(newIndivid.radius*0.5f) + newIndivid.radius*0.5f
            ,Rand::randFloat(newIndivid.radius*0.8f) + newIndivid.radius*0.5f);
        pos *= 2.0f;
        pos += center;
        newIndivid.position = pos;
        _individuals.emplace_back(newIndivid);
    }


    if(specialGuest != Individ::Human)
    {
        auto& specials = _defaultIndivids[side][specialGuest];
        auto newIndivid = specials[Rand::randInt(0, specials.size())];
        //auto newIndivid = _defaultIndivids[side][specialGuest];
        newIndivid.position  = center;
        _individuals.emplace_back(newIndivid);
    }


    std::sort(_individuals.begin(), _individuals.end()
        , [](const Individ& a, const Individ& b)
    {
        return a.position.y < b.position.y;
    });
}


}
