import os, string, xml.etree.ElementTree as ET
from PIL import Image

if __name__ == "__main__":
    anim_types = ('walk', 'happy')
    scale = 1
    info = {
    'general' : { 'scale' : '0.5', 'side' : "army", 'type' : "hero", 'speed' : "30", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {} },
    'soldier' : { 'scale' : '0.5','side' : "army", 'type' : "human", 'speed' : "50", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'black_glass' : {'scale' : '0.3', 'side' : "hippie", 'type' : "hero", 'speed' : "40", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'hippie1' : { 'scale' : '0.5','side' : "hippie", 'type' : "human", 'speed' : "40", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'guitarmen' : {'scale' : '0.25', 'side' : "hippie", 'type' : "human", 'speed' : "30", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'girl' : { 'scale' : '0.2','side' : "hippie", 'type' : "human", 'speed' : "40", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'soldier2' : {'scale' : '0.2', 'side' : "army", 'type' : "human", 'speed' : "40", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'tank' : { 'scale' : '0.20','side' : "army", 'type' : "vehicle", 'speed' : "40", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}},
    'bus' : { 'scale' : '0.20','side' : "hippie", 'type' : "vehicle", 'speed' : "50", 'radius' : "80", 'health' : "1", 'walk' : {}, 'happy' : {}}
    }

    atlas_size = (0,0)
    for t in info.keys():
        unit_scale = float(info[t]['scale'])
        for anim in anim_types:
            i = 0
            files = []
            while(True):
                i += 1
                path = t + "_" + anim + "_" + str(i) + ".png"
                if not os.path.isfile(path): break
                files.append(path)

            if not len(files) :
                if 'frame_size' in info[t][anim_types[0]].keys():
                    atlas_size = (atlas_size[0], atlas_size[1] + info[t][anim_types[0]]['frame_size'][1])

                continue

            size = Image.open(files[0],'r').size
            info[t][anim]['frame_size'] = size
            if 'resize' in info[t][anim].keys():
                info[t][anim]['frame_size'] = info[t][anim]['resize'] #size

            total_scale = scale * unit_scale
            frame_size_original = info[t][anim]['frame_size']
            info[t][anim]['frame_size'] = (int(frame_size_original[0] * total_scale), int(frame_size_original[1] * total_scale))
            info[t]['radius'] = float(info[t][anim]['frame_size'][1]) * 0.5 #float(info[t]['radius']) * total_scale
            info[t][anim]['frames'] = len(files)
            info[t][anim]['files'] = files
            atlas_size = (atlas_size[0] if atlas_size[0] >= info[t][anim]['frame_size'][0] * len(files) else info[t][anim]['frame_size'][0] * len(files), atlas_size[1] + info[t][anim]['frame_size'][1])

    atlas = Image.new('RGBA', atlas_size, (255, 255, 255, 0))
    y_offset = 0

    f = open('units.xml', 'w')
    f.write('<units></units>')
    f.close()

    xml_tree = ET.parse('units.xml')
    xml_root = xml_tree.getroot()

    for child in xml_root:
        xml_root.remove(child)

    for t in info.keys():
        unit_node = ET.Element(t)
        unit_node.set('side', info[t]['side'])
        unit_node.set('type', info[t]['type'])
        unit_node.set('speed', info[t]['speed'])
        unit_node.set('radius', str(info[t]['radius']))
        unit_node.set('health', info[t]['health'])

        for anim in anim_types:
            x_offset = 0
            anim_info = info[t][anim]

            if not 'files' in anim_info.keys():
                if not 'files' in info[t][anim_types[0]].keys():
                    continue
                else: anim_info = info[t][anim_types[0]]

            anim_node = ET.Element(anim)
            anim_node.set('fps', str(len(anim_info['files'])))
            anim_node.set('frameCount', str(len(anim_info['files'])))
            anim_node.set('frameSize', ' '.join( [   str(anim_info['frame_size'][0]), str(anim_info['frame_size'][1])]  ))
            anim_node.set('offset', ' '.join( [str(x_offset), str(y_offset)]))


            for f in anim_info['files']:
                frame = Image.open(f,'r')
                frame = frame.resize(anim_info['frame_size'], Image.ANTIALIAS)
                atlas.paste(frame, (x_offset, y_offset))
                x_offset += anim_info['frame_size'][0]

            y_offset += anim_info['frame_size'][1]
            unit_node.append(anim_node)


        xml_root.append(unit_node)

    atlas.save('atlas.png')
    xml_tree.write('units.xml')
