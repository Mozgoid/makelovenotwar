#pragma once

#include "cinder/Xml.h"

#include <vector>
#include <unordered_map>
#include <mutex>

namespace game
{
    struct DrawJob
    {
        ci::Area   textureArea;
        ci::Rectf  renderRect;

        DrawJob(ci::Area   a, ci::Rectf d)
            : textureArea(a), renderRect(d)
        { }
    };


    struct AnimationInfo
    {
        float       frameTime;
        unsigned    frameCount;
        ci::Vec2f   textureOffset;
        ci::Vec2f   frameSize;
        //ci::Vec2f   drawSize;

        AnimationInfo(const ci::XmlTree& xml);

    };


    class Animation
    {
    public:
        Animation();
        Animation(AnimationInfo* anim);

        void update(float dt);
        void createDrawTasks(ci::Vec2f pos) const;

    private:
        AnimationInfo* _anim;
        unsigned  _frame;
        float     _time;
    };

    enum class ConflictSide_e { Hippie, Army, Neytral };


struct Individ 
{
public:
    enum Type_e { Human, Vehicle, Hero, TypeCount };


    Animation       animation[2];
    int             animIndex;
    float           speed;
    ci::Vec2f       position;
    ConflictSide_e  side;
    Type_e          type;
    float           radius;


    Individ() {};
    Individ(const ci::XmlTree& xml);
};


//==============================================================================
//                                 Game
//==============================================================================
class Game
{
public:
    Game();
    void init();
    void update(float dt);
    void draw() const;
    void tryAddHippieSquad(ci::Vec2f position);

private:
    void addArmySquad();
    void createDefaultIndivids(const ci::XmlTree& xml);
    void add(ConflictSide_e side, unsigned count
        , Individ::Type_e specialGuest, ci::Vec2f center);

    float _hippiePoints;
    float _armyPoints;
    float _pointsToWar;
    float _timeFromLastSoldiersAppear;
    float _timeToCleanup;
    std::mutex      _gameMutex;

    std::unordered_map<ConflictSide_e, std::unordered_map<Individ::Type_e, std::vector<Individ>>> 
        _defaultIndivids;

    std::vector<std::unique_ptr<AnimationInfo>> _animations;
    std::vector<Individ> _individuals;
};



}