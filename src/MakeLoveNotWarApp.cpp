#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/Rand.h"
#include "cinder/gl/Texture.h"

#include "gameClasses.h"

#include <vector>
#include <chrono>
#include <future>

using namespace ci;
using namespace ci::app;
using namespace std::chrono;
using namespace game;


std::vector<DrawJob> g_drawJobs;


class MakeLoveNotWarApp : public AppNative {
public:
    void setup() override;
    void mouseDown( MouseEvent event ) override;	
    void update() override;
    void draw() override;
private:
    Game           _game;
    gl::Texture    _atlas;
    time_point<high_resolution_clock> _lastFramePoint;
    std::future<void> _updateTask;
    std::vector<DrawJob> _drawJobs;
};


void MakeLoveNotWarApp::setup()
{
    setFullScreen(true);
    Rand::randomize();
    
    _atlas = gl::Texture(loadImage(loadAsset("atlas.png")));
    _lastFramePoint = high_resolution_clock::now();
    _game.init();
    gl::enableAlphaTest();
    gl::disableDepthRead ();
 	gl::disableDepthWrite ();
}


void MakeLoveNotWarApp::mouseDown( MouseEvent event )
{
    _game.tryAddHippieSquad(event.getPos());
}


void MakeLoveNotWarApp::update()
{
    const auto now = high_resolution_clock::now();
    auto dt = duration_cast<duration<float>>(now - _lastFramePoint).count();
    if(dt > 0.5f)
    {
        dt = 0.5f;
    }
    _lastFramePoint = now;

    if(_updateTask.valid())
    {
        _updateTask.wait();
    }

    _drawJobs = std::move(g_drawJobs);
    g_drawJobs = std::vector<DrawJob>();

    _updateTask = std::async(std::launch::async, 
        [this](float dt) { _game.update(dt);}, dt);
}


void MakeLoveNotWarApp::draw()
{
    gl::clear( Color( 1, 0, 0 ) );

    const auto jobsSize = _drawJobs.size();

    for(const auto& d : _drawJobs)
    {
        gl::draw(_atlas, d.textureArea, d.renderRect);
    }

    static auto font = Font( "Quicksand Book Regular", 35.0f );

    gl::drawString( toString(getAverageFps()), Vec2f( 10.0f, 10.0f )
        , Color::white(), font );
    
    gl::drawString( toString(jobsSize), Vec2f( 10.0f, 40.0f )
        , Color::white(), font );
}


CINDER_APP_NATIVE( MakeLoveNotWarApp, RendererGl )
